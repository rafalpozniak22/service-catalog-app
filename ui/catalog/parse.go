package catalog

import (
	"io/ioutil"
	"fmt"
	yaml "gopkg.in/yaml.v2"
)

// Parse will parse a file and return the yaml representation
func Parse(file string) (*ServiceCatalog, error) {

	yamlFile, err := ioutil.ReadFile("service-catalog.yml")
	if err != nil {
		return nil, err
	}

	var t ServiceCatalogYAML
	err = yaml.Unmarshal(yamlFile, &t)
	if err != nil {
		return nil, err
	}

	catalog, err := process(t)
	if err != nil {
		return nil, err
	}

	return catalog, nil
}

func ParseGitLabTeams(file string) (*GitLabTeams, error) {
	yamlFile, err := ioutil.ReadFile("team.yml")
	if err != nil {
		return nil, err
	}

	var t GitLabTeamsYAML
	err = yaml.Unmarshal(yamlFile, &t)
	if err != nil {
		fmt.Println(err)
		return nil, err
	}
	
	GitLabTeams, err := processGitLabTeams(t)
	if err != nil {
		fmt.Println(err)
		return nil, err
	}

	return GitLabTeams, nil
}
