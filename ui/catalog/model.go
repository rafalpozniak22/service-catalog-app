package catalog

type ServiceCatalogYAML struct {
	Teams    []TeamYAML    `yaml:"teams"`
	Tiers    []TierYAML    `yaml:"tiers"`
	Services []ServiceYAML `yaml:"services"`
}

type ServiceCatalog struct {
	ServiceCatalogYAML
	Teams    []Team
	Tiers    []Tier
	Services []Service
}

type TeamYAML struct {
	Name                string
	URL                 string
	ManagerSlug         string `yaml:"manager_slug"`
	SlackChannel        string `yaml:"slack_channel"`
	EngagementPolicyURL string `yaml:"engagement_policy"`
	OnCallScheduleURL   string `yaml:"oncall_schedule"`
}

type TierYAML struct {
	Name string
}

type ServiceYAML struct {
	Name            string
	Tier            string
	Teams           []string
	Label           string
	Business        BusinessServiceYAML
	Technical       TechnicalServiceYAML
	Infrastructure  InfrastructureServiceYAML
	Operations      OperationsServiceYAML
	Observability   ObservabilityServiceYAML
}

type SLAServiceYAML struct {
	Availability    string `yaml:"availability"`
	ResponseTime    string `yaml:"response_time"`
}

type BusinessServiceYAML struct {
	Requirement      string 
	Customers        []string 
	BusinessValue    string  `yaml:"business_value"`
	SLA              SLAServiceYAML 
}

type DocumentServiceYAML struct {
	Design       string
	Architecture string
	Service      []string
	Security     string
}

type DependencyServiceYAML struct {
	Service      string
}

type ConfigurationServiceYAML struct {
	ConfigName    string `yaml:"config_name"`
	Location      string `yaml:"location"`
}

type ScalabilityServiceYAML struct {
	Node         string
	Bound        string
	Scalable     string
}

type SecurityServiceYAML struct {
	SecurityReviewed   bool      `yaml:"security_reviewed"`
	DataClassification string    `yaml:"data_classification"`
	SecurityIncidents  []string  `yaml:"security_incidents"`
}

type TechnicalServiceYAML struct {
	ProjectURL     []string `yaml:"project"`
	Criticality    int
	SLX            string
	Documents      DocumentServiceYAML
	Dependencies   []DependencyServiceYAML
	Configurations []ConfigurationServiceYAML
	ChefRoles      []string `yaml:"chef_roles"`
	Scalability    ScalabilityServiceYAML
	Security       SecurityServiceYAML
	Logging        []LoggingServiceYAML
}

type AccessServiceYAML struct {
	AccountName    string
	AccessType     string
	Reviewed       bool
}

type InfrastructureServiceYAML struct {
	Provider     string
	Architecture string
	Provisioning string
	Accesses     []AccessServiceYAML
}

type OperationsServiceYAML struct {
	Maintenance    string
	Runbooks       []string
	Playbooks      []string
	ErrorBudget    string
}

type ObservabilityServiceYAML struct {
	Monitors MonitorsServiceYAML
}

type MonitorsServiceYAML struct {
	SentrySlug              string `yaml:"sentry_slug"`
	GrafanaFolder           string `yaml:"grafana_folder"`
	PrimaryGrafanaDashboard string `yaml:"primary_grafana_dashboard"`
}

type LoggingServiceYAML struct {
	Name      string
	Permalink string
}

type Team struct {
	TeamYAML
}

type Tier struct {
	TierYAML
}

type Service struct {
	ServiceYAML
	Tier  Tier
	Teams []Team
}

type GitLabTeamYAML struct {
	Slug        string `yaml:"slug"`
	Type      	string 
	Name 		string
	StartDate 	string `yaml:"start_date"`
	PlaceHolder bool   
	Locality 	string
	Country 	string
	Role 		string
	ReportsTo 	string `yaml:"reports_to"`
	Picture 	string
	Twitter 	string
	GitLab 		string
	Departments []string
	Expterise 	string
	Story 		string
	Blank       string
}

type GitLabTeam struct {
	GitLabTeamYAML
}

type GitLabTeams struct {
	GitLabTeamsYAML
	GitLabTeams []GitLabTeam
}

type GitLabTeamsYAML struct {
	GitLabTeams []GitLabTeamYAML `yaml:"GitLabTeams"`
}