package main

import (
	"net/http"

	"gitlab.com/gitlab-com/gl-infra/service-catalog-app/ui"
)

func main() {
	http.ListenAndServe(":8080", ui.Handler)
}
